//@flow
import React from 'react';
import Paper from '@material-ui/core/Paper';
import '../../App.css';

type Props = {
  xPosition: number,
  yPosition: number,
  width: number,
  height: number,
  mode: number
}

const Paddle = (props: Props) => {

  const width : string = props.width+'px';
  const height : string = props.height+'px';

  const left : string = props.xPosition+'px';
  const top : string = props.yPosition+'px';

  const style = {
    'width': width,
    'height': height,
    'left': left,
    'top': top,
    'position': 'absolute'
  }

  const paddleImage = (mode) => {
    mode = 0
    if(mode === 0){
      return <Paper elevation={0}
              style={style}
            />
    }

  }

  return(
    <div className="Paddle">
      {/* <Paper elevation={0}
        style={style}
      /> */}

      {paddleImage(props.mode)}
    </div>
  )
}

export default Paddle;