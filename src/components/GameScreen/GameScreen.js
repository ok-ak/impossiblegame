//@flow
import React, { Component } from 'react';
import Button from '@material-ui/core/Button'
import Paddle from '../Paddle/Paddle';
import Ball from '../Ball/Ball';

type Props = {}

type State = {
  player1X: number,
  player1Y: number,
  player1Mode: number,
  player1Score: number,
  player2X: number,
  player2Y: number,
  player2Mode: number,
  player2Score: number,
  paddleWidth: number, 
  paddleHeight: number,
  ballX: number,
  ballY: number,
  ballVectorX: number,
  ballVectorY: number,
  ballWidth: number,
  ballHeight: number,
  ballMode: number,
  selfDestructTimer: number,
  roundsCount: number,
  gamesCount: number,
  giveUpCount: number,
  screenWidth: number,
  screenHeight: number
}

class GameScreen extends Component<Props, State> {

  state = {
    player1X: 20,
    player1Y: 40,
    player1Mode: 0,
    player1Score: 0,
    player2X: 100,
    player2Y: 200,
    player2Mode: 0,
    player2Score: 0,
    paddleWidth: 24, 
    paddleHeight: 100,
    ballX: 100,
    ballY: 100,
    ballVectorX: 1,
    ballVectorY: 1,
    ballWidth: 24,
    ballHeight:24,
    ballMode: 0,
    selfDestructTimer: 10,
    roundsCount: 0,
    gamesCount: 0,
    giveUpCount: 0,
    screenWidth: 200,
    screenHeight: 100
  }

  styles = {
    'position': 'absolute',
    'top': '50%',
    'left': '50%',
    'transform': 'translate(-50%, -50%)',
    'height': '100%',
    'width': '100%',
    'display': 'flex',
    'flexDirection': 'row',
    'justifyContent': 'center',
    'alignItems': 'stretch',
    'backgroundColor': '#3f3f3f'
  }
  
  backDropStyle = {
    'display': 'flex',
    'flexDirection': 'column',
    'justifyContent': 'space-between',
    'alignItems': 'center',
    'width': '100%'
  }
  
  scoreScreenStyle = {
    'display': 'flex',
    'flexDirection': 'row',
    'justifyContent': 'space-evenly',
    'marginTop': '24px',
    'width': '100%'
  }
  
  playerScoreStyle = {
    'display': 'flex',
    'flexDirection': 'column',
    'alignItems': 'center',
    'color': 'white',
    'fontSize': '24px'
  }
  
  playerScoreNumberStyle = {
    'marginTop': '24px',
    'fontSize': '48px'
  }
  
  menuStyle = {
    'color': 'white',
    'display': 'flex',
    'flexDirection': 'column',
    'alignItems': 'center',
    'marginBottom': '24px'
  }

  buttonStyle ={
    'marginBottom': '24px'
  }
  
  componentDidMount(){
    window.addEventListener('resize', this.screenSizeHandler.bind(this));
    let [screenWidth, screenHeight] = this.screenSizeHandler();
    this.cpuPaddleHandler(screenWidth, screenHeight);
  }

  componentDidUpdate(prevProps: Props, prevState: State) {
    if(prevState.screenHeight !== this.state.screenHeight || prevState.screenWidth !== this.state.screenWidth){
      this.cpuPaddleHandler(this.state.screenWidth, this.state.screenHeight);
    }
  }

  // componentWillUpdate(){
  //   this.cpuPaddleHandler(this.state.screenWidth, this.state.screenHeight);
  // }

  // shouldComponentUpdate(state: State, nextState: State){
    // let update: boolean = false;
    // if(props !== nextProps || state != nextState){
    //   update = true;
    //   let newScreenWidth: number = nextState.screenWidth;
    //   let newScreenHeight: number = nextState.screenHeight;
    //   this.cpuPaddleHandler(newScreenWidth, newScreenHeight);
    // }
    // return update;
  //   return true;
  // }

  // shouldComponentUpdate

  componentWillUnmount(){
    window.removeEventListener('resize', this.screenSizeHandler);
  }
  
  restartGameHandler = () => {
    console.log("Restart game button pressed! Initializing a new game!")
    let newGamesCount = this.state.gamesCount;
    let newRoundsCount = this.state.roundsCount;
    let newPlayer1Mode = 0;
    let newPlayer1Score = 0;
    let newPlayer2Mode = 0;
    let newPlayer2Score = 0;
    let newBallMode = 0;
    let newBallX = 30;
    let newBallY = 30;
    this.setState({
      gamesCount: newGamesCount,
      roundsCount: newRoundsCount,
      player1Mode: newPlayer1Mode,
      player1Score: newPlayer1Score,
      player2Mode: newPlayer2Mode,
      player2Score: newPlayer2Score,
      ballMode: newBallMode,
      ballX: newBallX,
      ballY: newBallY
    })
  }
  
  giveUpHandler = () => {
    console.log("I give up...make it stop!!!!")
    let newGiveUpCount = this.state.giveUpCount + 1;
    this.setState({giveUpCount: newGiveUpCount})
  }
  
  screenSizeHandler = () => {
    let newWidth : number = document.getElementById('screenContainer').clientWidth
    let newHeight : number = document.getElementById('screenContainer').clientHeight
    console.log('screen size', newWidth, newHeight)
    this.setState({
      screenWidth: newWidth,
      screenHeight: newHeight
    })
    
    this.setState({
      screenWidth: newWidth,
      screenHeight: newHeight
    })

    return [newWidth, newHeight]
  }
  
  cpuPaddleHandler = (screenWidth: number, screenHeight: number) => {
    let paddleTopLimitY: number = this.state.paddleHeight / 2
    let paddleBottomLimitY: number = screenHeight - this.state.paddleHeight / 2
    let paddleY: number = this.state.ballY + this.state.ballHeight / 2

    //horizontal logic for player paddle
    let newX = screenWidth - this.state.paddleWidth * 2

    //vertical logic for player paddle
    let newY: number = 0
    if (paddleY > paddleBottomLimitY){
      newY = screenHeight - this.state.paddleHeight
    } else if (paddleY > paddleTopLimitY){
      newY = paddleY - this.state.paddleHeight / 2
    }
    
    // console.log('cpu paddle handler', newX, newY)

    this.setState({
      player2X: newX,
      player2Y: newY
    })
  }
  
  mousePositionHandler = (e: Object) => {
    let paddleTopLimitY: number = this.state.paddleHeight / 2
    let paddleBottomLimitY: number = this.state.screenHeight - this.state.paddleHeight / 2
    let mouseY: number = e.nativeEvent.clientY

    //horizontal logic for player paddle
    let newX = this.state.paddleWidth

    //vertical logic for player paddle
    let newY: number = 0
    if (mouseY > paddleBottomLimitY){
      newY = this.state.screenHeight - this.state.paddleHeight
    } else if (mouseY > paddleTopLimitY){
      newY = mouseY - this.state.paddleHeight / 2
    }

    this.setState({
      player1X: newX,
      player1Y: newY
    })
  }

  render(){
    return(
      <div className="GameScreen"
        onMouseMove = {this.mousePositionHandler.bind(this)}
        id="screenContainer"
        style={this.styles}>
        <div className="backDrop" style={this.backDropStyle}>
          <div className="score" style={this.scoreScreenStyle}>
            <div className="playerScore" style={this.playerScoreStyle}>
              <div>Player 1</div>
              <div style={this.playerScoreNumberStyle}>{this.state.player1Score}</div>
            </div>
            <div className="playerScore" style={this.playerScoreStyle}>
              <div>Player 2</div>
              <div style={this.playerScoreNumberStyle}>{this.state.player2Score}</div>
            </div>
          </div>
          <div className="menu" style={this.menuStyle}>
            <Button color={'inherit'} style={this.buttonStyle} onClick={this.giveUpHandler}>I Give Up!</Button>
            <div>
              Like the game? Checkout my GitLab Repo! <a href="http:\\www.gitlab.com/ok-ak/ImpossibleGame">Click Here</a>
            </div>
          </div>
        </div>
        
        <Paddle
          xPosition={this.state.player1X}
          yPosition={this.state.player1Y}
          width={this.state.paddleWidth}
          height={this.state.paddleHeight}        
          mode={this.state.player1Mode}
          />
        <Paddle
          xPosition={this.state.player2X}
          yPosition={this.state.player2Y}
          width={this.state.paddleWidth}
          height={this.state.paddleHeight}        
          mode={this.state.player2Mode}
          />
        <Ball
          xPosition={this.state.ballX}
          yPosition={this.state.ballY}
          width={this.state.ballWidth}
          height={this.state.ballHeight}
          mode={this.state.ballMode}/>
      </div>
    )
  }
}

export default GameScreen;