//@flow
import React from 'react';
import {Button, Grid, Typography, Paper} from '@material-ui/core';
import {Link} from 'react-router-dom';

type Props = {
  selfDestructTimerHandler: any;
  counter: number;
};

class ErrorPage extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
  }

  incrementCount = setInterval(() => {
    this.props.selfDestructTimerHandler()
  }, 1000);

  render(){
    return (
      <div className="ErrorPage">
        <Grid
          container
          direction="column"
          justify="center"
          alignItems="center"
        >
          <Grid
            item xs={8}
          >
            <Paper className="p-l-16 p-r-16 p-t-16 p-b-16" elevation={0}>
              <Typography
                variant="headline"
                align="center"
              >
                {'WARNING! CRITIAL SYSTEM ERROR HAS OCCURRED.'}
              </Typography>
            </Paper>
          </Grid>
          <Grid
            item xs={8}
          >
            <Paper className="p-l-16 p-r-16 p-t-16 p-b-16" elevation={0}>
              <Typography
                variant="subheading"
                align="center"
              >
                {'Initiating self-destruct sequence in ' + this.props.counter}
              </Typography>
            </Paper>
          </Grid>
          <Grid
            item xs={8}
          >
            <Link to="/" className="no-underline-link">
              <Button>
                {'Panic Button'}
              </Button>
            </Link>
          </Grid>
        </Grid>
      </div>
    )
  }
}

// React.render(
// 	<ErrorPage/>, 
// 	document.body
// );
export default ErrorPage;