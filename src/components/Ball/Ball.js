//@flow
import React from 'react';
import Paper from '@material-ui/core/Paper';
import '../../App.css';

type Props = {
  xPosition: number,
  yPosition: number,
  width: number,
  height: number,
  mode: number
}

const Ball = (props: Props) => {
  const top : string = props.yPosition+'px';
  const left : string = props.xPosition+'px';

  const width : string = props.width+'px';
  const height : string = props.height+'px';

  const style = {
    'width': width,
    'height': height,
    'top': top,
    'left': left,
    'position': 'absolute'
  }

  return(
    <div className="Paddle">
      <Paper elevation={0}
        style={style}
      />
    </div>
  )
}

export default Ball;