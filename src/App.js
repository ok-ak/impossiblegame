//@flow
import React, { Component } from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';
import GameScreen from './components/GameScreen/GameScreen';
import ErrorPage from './components/ErrorPage/ErrorPage';


type Props = {}

class App extends Component<Props> {

  // selfDestructTimerHandler = () => {
  //   console.log("Self destruct has been activated!");
  //   let time : number = this.state.selfDestructTimer;
  //   if(time > 1){
  //     time -= 1;
  //   } else {
  //     time /= 2;
  //   }
  //   this.setState({selfDestructTimer: time})
  // }



  render() {
    return (
      <div className="App">
        <Router>
          <Switch>
            <Route
              path="/" exact
              render = {()=>
                <GameScreen />
              }
            />
            {/* <Route
              render={()=>
                <ErrorPage
                  selfDestructTimerHandler = {this.selfDestructTimerHandler}
                  counter = {this.state.selfDestructTimer}
                />
              }
            /> */}
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;