# Impossible Game

The kind of game that makes Dark Souls look like a vacation in a tropical region full of all you can eat buffet, free drinks and endless sunshine.

But...why you ask?

Remember those fleeting moments as a younger you when you could not fathom how the boss managed to eek out a last minute win with a miraculous series of endless health potions or sudden bad luck rolls as you watched helplessly as your character's health fell, stumbled, then dribbled out?

In those dark tumultous periods of (╯°□°）╯︵ ┻━┻ and possibly even T_T there was that whisper of a thought...

> Is the computer cheating?

Great Question!
The answer is now a definitive yes! Good luck!


Note:
This is a pet project to try out react with something fun. Maaad props to all of those nostalgic games were you couldn't help but feel the computer. was. cheating. amiright or am i right? No way my Lvl. 1 character with no items was underpowered for that Lvl. 9000 boss :D

### Deployment
Make sure you have surge installed as a dependancy in your package.json (ref surge.sh for more information)
Then at the root folder of your project:
`npm run build`
`cd build`
`surge`
`ImpossibleGame.surge.sh`
Now the project is deployed : )